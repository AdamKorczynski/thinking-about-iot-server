## Installation 

npm install -g json-server

## Starting the server

json-server --watch offers.json

## Re-generating test data
Execute .\generate-date.ps1 in a PowerShell session.

## json-server home
https://github.com/typicode/json-server
