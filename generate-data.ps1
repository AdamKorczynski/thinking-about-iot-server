

$OfferId = 435234;
$CustomerId = 234523;

$States = $('created', 'activated', 'extended', 'expired')
$Locations = [PSCustomObject]@{ 
    Address = "Queen Victoria Building, 69/455 George Street, Sydney"
    PlaceId = "2345234523452"
},
[PSCustomObject]@{ 
    Address = "Bondi Junction"
    PlaceId = "8573585693943"
},
[PSCustomObject]@{ 
    Address = "2/525 George Street, Sydney"
    PlaceId = "9385377574828"
},
[PSCustomObject]@{ 
    Address = "Pacific Power Building, 201 Elizabeth Street, Sydney"
    PlaceId = "9257254737447"
},
[PSCustomObject]@{ 
    Address = "1/11-17 York Street, Sydney"
    PlaceId = "6352546548366"
},
[PSCustomObject]@{ 
    Address = "Capitol Square, G01/730-742 George Street, Haymarket"
    PlaceId = "3857372906443"
}

Function Create-Offers
{
    For ($i=0; $i -le 100; $i++) 
    {
        $CustomerId = Get-Random -Minimum 23452365 -Maximum 89452335
        $GenereatedTime = (Get-Date).AddMinutes((Get-Random -Minimum -6000 -Maximum 0))
        $ExpirationTime = $GenereatedTime.AddMinutes(60)
        $State = $States[(Get-Random -Minimum 0 -Maximum 3)]
        $Location = $Locations[(Get-Random -Minimum 0 -Maximum 5)]
        
        [PSCustomObject]@{
            OfferId = $OfferId
            CustomerId = $CustomerId
            PlaceId = $Location.PlaceId
            PlaceName = 'Starbucks'
            PlaceAddress = $Location.Address
            GeneratedTime = $GenereatedTime.ToString('s')
            ExpirationTime = $ExpirationTime.ToString('s')
            State = $State
        } | Write-Output

        $OfferId++;
        $CustomerId++;
    }
}

$Offers = Create-Offers
$Response = [PSCustomObject]@{
    offer = $Offers
}

$Response | ConvertTo-Json | Out-File -FilePath offers.json -Encoding ASCII